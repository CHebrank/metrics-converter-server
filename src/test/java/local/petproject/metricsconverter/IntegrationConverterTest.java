package local.petproject.metricsconverter;

import local.petproject.metricsconverter.datasets.AltmetricDataset;
import local.petproject.metricsconverter.repositories.AltmetricRepository;
import local.petproject.metricsconverter.repositories.RepositoryService;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class IntegrationConverterTest {

    @Autowired
    RepositoryService repositoryService;

    @Autowired
    AltmetricRepository altmetricRepository;

    @Test
    public void convertAltmetricData() {
        repositoryService.convertRawDataA(1, 10);
        List<AltmetricDataset> result = altmetricRepository.findAll();
        Assert.assertEquals(10, result.size());
    }

}
