package local.petproject.metricsconverter.fileIO;

import local.petproject.metricsconverter.datasets.WosDataset;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class DataImporterTest {

    @Test
    public void checkColCount() throws IOException {
        DataImporter importer = new DataImporter("src/main/resources/files/WoS-1.txt");
        int result = importer.countColnames();
        assertEquals(68, result);
    }

    @Test
    public void checkDatasetCount() throws IOException {
        DataImporter importer = new DataImporter("src/main/resources/files/WoS-1.txt");
        List theList = importer.importDatasets();
        assertEquals(501, theList.size());
    }

    @Test
    public void checkDataConversion() throws IOException {
        DataImporter importer = new DataImporter("src/main/resources/files/WoS-1.txt");
        List imported = importer.importDatasets();
        assertEquals(501, imported.size());
        List<WosDataset> converted = importer.convertToWosDatasets(imported);
        WosDataset testDataset = converted.get(0);
        assertEquals("J", testDataset.getPublicationType());
        assertEquals("10.1103/PhysRevB.96.224434", testDataset.getDoi());
    }
}
