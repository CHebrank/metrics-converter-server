package local.petproject.metricsconverter.fileIO;

import local.petproject.metricsconverter.datasets.AltmetricDataset;
import local.petproject.metricsconverter.datasets.RawDataA;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;
import java.util.HashMap;

public class DataConverterTest {

    RawDataA dataA = new RawDataA("{\"title\":\"Observation of chemically protected polydimethylsiloxane: towards crack-free PDMS\",\"doi\":\"10.1039/c7sm01457k\",\"pmid\":\"28920620\",\"cohorts\":{\"pub\":1},\"cited_by_posts_count\":1,\"cited_by_tweeters_count\":1,\"cited_by_accounts_count\":1,\"last_updated\":1505064198,\"score\":0.25,\"history\":{\"1y\":0,\"6m\":0,\"3m\":0,\"1m\":0,\"1w\":0,\"6d\":0,\"5d\":0,\"4d\":0,\"3d\":0,\"2d\":0,\"1d\":0,\"at\":0.25},\"added_on\":1505064212,\"published_on\":1483228800,\"readers\":{\"citeulike\":\"0\",\"mendeley\":\"16\",\"connotea\":\"0\"},\"readers_count\":16}");

    @Test
    public void testGetArray() {
        String[] array = DataConverter.getAttributeArray(dataA.getContent());
        Assert.assertEquals("\"pmid\":\"28920620\"", array[2]);
    }

    @Test
    public void testMapping() {
        HashMap<String, String> map = DataConverter.createMapping(DataConverter.getAttributeArray(dataA.getContent()));
        Assert.assertEquals("10.1039/c7sm01457k", map.get("doi"));
    }

    @Test
    public void testAltmetricDataset() {
        HashMap<String, String> map = DataConverter.createMapping(DataConverter.getAttributeArray(dataA.getContent()));
        AltmetricDataset amd = DataConverter.convertToAltmetric(map);

        Assert.assertEquals(1, amd.getCohortsPub());
        Assert.assertEquals(1, amd.getCitedPosts());
        Assert.assertEquals(1, amd.getCitedTweeters());
        Assert.assertEquals(0.25, amd.getScore(), 0.001);
        Assert.assertEquals(0, amd.getHistory1y(), 0.001);
        Assert.assertEquals(new Date(1505064212L * 1000), amd.getAddedOn());
        Assert.assertEquals(16, amd.getReaderMendeley());
    }

}
