package local.petproject.metricsconverter.fileIO;

import local.petproject.metricsconverter.datasets.WosDataset;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DataImporter {

    private String fileName;
    private String currentLine;
    private FileReader reader;
    private BufferedReader bReader;


    public DataImporter(String fileName) throws FileNotFoundException {
        this.fileName = fileName;
        this.reader = new FileReader(fileName);
        this.bReader = new BufferedReader(reader);
    }
    public DataImporter() {}


    public int countColnames() throws IOException {
        currentLine = bReader.readLine();
        bReader.close();
        String[] headings = currentLine.split("\t");
        return headings.length;
    }

    public List<String[]> importDatasets() throws IOException {
        List<String[]> datasetList = new ArrayList<>();
        while ((currentLine = bReader.readLine()) != null) {
            String[] line = currentLine.split("\t");
            datasetList.add(line);
        }
        bReader.close();
        return datasetList;
    }

    public List<WosDataset> convertToWosDatasets(List<String[]> listIn) {
        List<WosDataset> convertedData = new ArrayList<>();
        for (int j = 1; j < listIn.size(); j++) {
            WosDataset currentDataset = new WosDataset();
            try {
                currentDataset.setPublicationType(listIn.get(j)[0]);
                currentDataset.setAuthor(listIn.get(j)[5]);
                currentDataset.setTitle(listIn.get(j)[8]);
                currentDataset.setJournal(listIn.get(j)[9]);
                currentDataset.setDocType(listIn.get(j)[13]);
                currentDataset.setAddress(listIn.get(j)[22]);
                currentDataset.setReprAddress(listIn.get(j)[23]);
                currentDataset.setCitedCountCCTCC(Integer.valueOf(listIn.get(j)[31]));
                currentDataset.setTotalCitedCountTTCC(Integer.valueOf(listIn.get(j)[32]));
                currentDataset.setPublisher(listIn.get(j)[35]);
                currentDataset.setPubDate(listIn.get(j)[43]);
                currentDataset.setPubYear(listIn.get(j)[44]);
                currentDataset.setDoi(listIn.get(j)[54]);
                currentDataset.setSubject(listIn.get(j)[60]);
                if (listIn.get(j).length > 64) {
                    currentDataset.setOpenAccessWoS(listIn.get(j)[64]);
                }
            } catch (IndexOutOfBoundsException exception){
                System.out.println("Fields missing in dataset " + listIn.get(j)[8]);
            } catch (NumberFormatException numException){
                System.out.println("Invalid numbers in dataset " + listIn.get(j)[8]);
            }
            convertedData.add(currentDataset);
        }
        return convertedData;
    }


}
