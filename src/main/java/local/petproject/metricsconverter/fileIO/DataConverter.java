package local.petproject.metricsconverter.fileIO;

import local.petproject.metricsconverter.datasets.AltmetricDataset;
import local.petproject.metricsconverter.datasets.RawDataU;
import local.petproject.metricsconverter.datasets.UnpaywallDataset;

import java.util.Date;
import java.util.HashMap;

public class DataConverter {

    /************************** Altmetric Data **********************************/
    /**
     * Step 1a: Convert stringified JSON to array of fields/attributes by splitting at every comma
     * and removing extra brackets
     * @param inputData: JSON as string
     * @return array of separated JSON fields
     */
    public static String[] getAttributeArray(String inputData) {
        String[] attributes = inputData.split(",");
        for (int i = 0; i < attributes.length; i++) {
            String currentField = attributes[i];
            if (currentField.contains("{")) {
                currentField = currentField.substring(currentField.indexOf('{') + 1);
            }
            if (currentField.contains("}")) {
                currentField = currentField.substring(0, currentField.length()-1);
            }
            attributes[i] = currentField;
        }
        return attributes;
    }

    /**
     * Step 2: Convert attributes array into a HashMap of strings with key-value pairs
     * by splitting field-name from field-value at every ":"
     * Note: it is necessary to "hide" DOIs that contain ":" before the splitting step
     * @param attributes: array of separated JSON fields
     * @return map containing field-values by field-names
     */
    public static HashMap<String, String> createMapping(String[] attributes) {
        HashMap<String, String> attributeMap = new HashMap<>();
        boolean substitudedDoi = false;                         // check if doi with : was hidden
        for (int i = 0; i < attributes.length; i++) {
            String currentField = attributes[i];
            if (currentField.contains(":")) {
                // special case, contained in doi (e.g. "DOI : 10.10000/abc:de"), so hide it in doi by changing
                // the ':' to '§' temporarily
                if (currentField.contains("doi") && (currentField.indexOf(':') != currentField.lastIndexOf(':') )) {
                    StringBuilder change = new StringBuilder(currentField);
                    change.replace(currentField.lastIndexOf(':'), currentField.lastIndexOf(':')+1, "§");
                    currentField = change.toString();
                    substitudedDoi = true;
                }

                // normal case, cut between field name and value
                String[] parts = currentField.split(":");
                if (parts[0].contains("\"") && parts[0].length() > 2) {
                    parts[0] = parts[0].substring(1, parts[0].length() - 1);
                }
                if (parts[1].contains("\"") && parts[1].length() > 2) {
                    parts[1] = parts[1].substring(1, parts[1].length() - 1);
                }

                // special case, if doi was hidden then fix it now
                if (substitudedDoi) {
                    parts[1] = parts[1].replace('§', ':');
                }
                attributeMap.put(parts[0], parts[1]);
            }
        }
        return attributeMap;
    }

    /**
     * Step 3a: Convert attribute-map to AltmetricDataset with suitable types chosen for each field
     * @param attributeMap: map containing field-values by field-names
     * @return AltmetricDataset ready for being saved to the database, or null if no doi was contained
     */
    public static AltmetricDataset convertToAltmetric(HashMap<String, String> attributeMap) {
        if (attributeMap.containsKey("doi")) {
            AltmetricDataset dataset = new AltmetricDataset(attributeMap.get("doi"));
            // cohorts
            if (attributeMap.containsKey("pub")) {
                dataset.setCohortsPub(Integer.valueOf(attributeMap.get("pub")));
            }
            if (attributeMap.containsKey("sci")) {
                dataset.setCohortsSci(Integer.valueOf(attributeMap.get("sci")));
            }
            if (attributeMap.containsKey("com")) {
                dataset.setCohortsCom(Integer.valueOf(attributeMap.get("com")));
            }
            if (attributeMap.containsKey("doc")) {
                dataset.setCohortsDoc(Integer.valueOf(attributeMap.get("doc")));
            }

            // cited bys
            if (attributeMap.containsKey("cited_by_posts_count")) {
                dataset.setCitedPosts(Integer.valueOf(attributeMap.get("cited_by_posts_count")));
            }
            if (attributeMap.containsKey("cited_by_delicious_count")) {
                dataset.setCitedDelicious(Integer.valueOf(attributeMap.get("cited_by_delicious_count")));
            }
            if (attributeMap.containsKey("cited_by_fbwalls_count")) {
                dataset.setCitedFbwalls(Integer.valueOf(attributeMap.get("cited_by_fbwalls_count")));
            }
            if (attributeMap.containsKey("cited_by_feeds_count")) {
                dataset.setCitedFeeds(Integer.valueOf(attributeMap.get("cited_by_feeds_count")));
            }
            if (attributeMap.containsKey("cited_by_forum_count")) {
                dataset.setCitedForum(Integer.valueOf(attributeMap.get("cited_by_forum_count")));
            }
            if (attributeMap.containsKey("cited_by_gplus_count")) {
                dataset.setCitedGplus(Integer.valueOf(attributeMap.get("cited_by_gplus_count")));
            }
            if (attributeMap.containsKey("cited_by_linkedin_count")) {
                dataset.setCitedLinkedin(Integer.valueOf(attributeMap.get("cited_by_linkedin_count")));
            }
            if (attributeMap.containsKey("cited_by_msm_count")) {
                dataset.setCitedMsm(Integer.valueOf(attributeMap.get("cited_by_msm_count")));
            }
            if (attributeMap.containsKey("cited_by_peer_review_sites_count")) {
                dataset.setCitedPeerrev(Integer.valueOf(attributeMap.get("cited_by_peer_review_sites_count")));
            }
            if (attributeMap.containsKey("cited_by_pinners_count")) {
                dataset.setCitedPinners(Integer.valueOf(attributeMap.get("cited_by_pinners_count")));
            }
            if (attributeMap.containsKey("cited_by_policies_count")) {
                dataset.setCitedPolicies(Integer.valueOf(attributeMap.get("cited_by_policies_count")));
            }
            if (attributeMap.containsKey("cited_by_qs_count")) {
                dataset.setCitedQs(Integer.valueOf(attributeMap.get("cited_by_qs_count")));
            }
            if (attributeMap.containsKey("cited_by_rdts_count")) {
                dataset.setCitedRdts(Integer.valueOf(attributeMap.get("cited_by_rdts_count")));
            }
            if (attributeMap.containsKey("cited_by_rh_count")) {
                dataset.setCitedRh(Integer.valueOf(attributeMap.get("cited_by_rh_count")));
            }
            if (attributeMap.containsKey("cited_by_tweeters_count")) {
                dataset.setCitedTweeters(Integer.valueOf(attributeMap.get("cited_by_tweeters_count")));
            }
            if (attributeMap.containsKey("cited_by_videos_count")) {
                dataset.setCitedVideos(Integer.valueOf(attributeMap.get("cited_by_videos_count")));
            }
            if (attributeMap.containsKey("cited_by_weibo_count")) {
                dataset.setCitedWeibo(Integer.valueOf(attributeMap.get("cited_by_weibo_count")));
            }
            if (attributeMap.containsKey("cited_by_wikipedia_count")) {
                dataset.setCitedWikipedia(Integer.valueOf(attributeMap.get("cited_by_wikipedia_count")));
            }

            // dates
            if (attributeMap.containsKey("last_updated")) {
                long epoch = Long.valueOf(attributeMap.get("last_updated"));
                dataset.setLastUpdated(new Date(epoch * 1000));
            }
            if (attributeMap.containsKey("added_on")) {
                long epoch = Long.valueOf(attributeMap.get("added_on"));
                dataset.setAddedOn(new Date(epoch * 1000));
            }

            // history
            if (attributeMap.containsKey("score")) {
                dataset.setScore(Double.valueOf(attributeMap.get("score")));
            }
            if (attributeMap.containsKey("1y")) {
                dataset.setHistory1y(Double.valueOf(attributeMap.get("1y")));
            }
            if (attributeMap.containsKey("6m")) {
                dataset.setHistory6m(Double.valueOf(attributeMap.get("6m")));
            }
            if (attributeMap.containsKey("3m")) {
                dataset.setHistory3m(Double.valueOf(attributeMap.get("3m")));
            }
            if (attributeMap.containsKey("at")) {
                dataset.setHistoryAT(Double.valueOf(attributeMap.get("at")));
            }

            // readers
            if (attributeMap.containsKey("readers_count")) {
                dataset.setReadersCount(Integer.valueOf(attributeMap.get("readers_count")));
            }
            if (attributeMap.containsKey("citeulike")) {
                dataset.setReaderCiteULike(Integer.valueOf(attributeMap.get("citeulike")));
            }
            if (attributeMap.containsKey("mendeley")) {
                dataset.setReaderMendeley(Integer.valueOf(attributeMap.get("mendeley")));
            }
            if (attributeMap.containsKey("connotea")) {
                dataset.setReaderConnotea(Integer.valueOf(attributeMap.get("connotea")));
            }

            return dataset;

        } else {
            System.out.println("keine DOI gefunden :(" + attributeMap.toString());
            return null;
        }
    }

    /*********************** Unpaywall Data *************************************/

    /**
     * Step 1u: Convert and simplify stringified JSON by deleting unnecessary information contained in arrays ([])
     * and converting to array of fields/attributes by splitting at every comma
     * @param inputData: JSON as string
     * @return array of separated JSON fields or null if errors with array deletion occurred
     */
    public static String[] getSimplifiedDataArrayU(RawDataU inputData){
        // find locations of arrays in data (oa_locations and authors)
        int open1 = inputData.getContent().indexOf('[');
        int close1 = inputData.getContent().indexOf(']');
        int open2 = -1;
        if (open1 > 0 && close1 > 0) {
            open2 = inputData.getContent().indexOf('[', open1 + 1);
        }

        // simplify string by removing those arrays
        StringBuilder simplified = new StringBuilder();
        String simpleString = null;
        if (open1 > -1 && close1 > -1 && open2 > -1) {
            simplified.append(inputData.getContent().substring(0, open1 - 2)); // remove includes : before [
            simplified.append(inputData.getContent().substring(close1 + 1, open2 - 2));
            simpleString = simplified.toString();
        } else if (open1 > -1 && close1 > -1) {
            simplified.append(inputData.getContent().substring(0, open1 - 2)); // remove includes : before [
            simplified.append(inputData.getContent().substring(close1 + 1));
            simpleString = simplified.toString();
        } else {
            System.out.println("error in deleting arrays: " + inputData.getContent());
        }

        // split remaining string by using same methods as for Altmetric data (step 1a)
        if (simpleString != null) {
            return getAttributeArray(simpleString);
        } else {
            return null;
        }
    }

    // Step 2 as above

    /**
     * Step 3u: Convert attribute-map to UnpaywallDataset with suitable types chosen for each field
     * @param attributeMap: map containing field-values by field-names
     * @return UnpaywallDataset ready for being saved to the database, or null if no doi was contained
     */
    public static UnpaywallDataset convertToUnpaywall(HashMap<String, String> attributeMap) {
        if (attributeMap.containsKey("doi")) {
            UnpaywallDataset dataset = new UnpaywallDataset(attributeMap.get("doi"));

            // best OA location
            if (attributeMap.containsKey("host_type")) {
                dataset.setBestHostType(attributeMap.get("host_type"));
            }
            if (attributeMap.containsKey("license")) {
                dataset.setBestLicense(attributeMap.get("license"));
            }
            if (attributeMap.containsKey("updated")){
                dataset.setBestUpdated(attributeMap.get("updated"));
            }
            if (attributeMap.containsKey("version")){
                dataset.setBestVersion(attributeMap.get("version"));
            }
            if (attributeMap.containsKey("is_best")){
                dataset.setBestIsBest(Boolean.valueOf(attributeMap.get("is_best")));
            }

            // other data
            if (attributeMap.containsKey("data_standard")) {
                dataset.setDataStandard(Integer.valueOf(attributeMap.get("data_standard")));
            }
            if (attributeMap.containsKey("is_oa")){
                dataset.setOa(Boolean.valueOf(attributeMap.get("is_oa")));
            }
            if (attributeMap.containsKey("journal_is_in_doaj")){
                dataset.setJournalDoaj(Boolean.valueOf(attributeMap.get("journal_is_in_doaj")));
            }
            if (attributeMap.containsKey("journal_is_oa")){
                dataset.setJournalOA(Boolean.valueOf(attributeMap.get("journal_is_oa")));
            }
            if (attributeMap.containsKey("published_date")){
                dataset.setPublishedDate(attributeMap.get("published_date"));
            }
            if (attributeMap.containsKey("updated")){
                dataset.setUpdated(attributeMap.get("updated"));
            }

            return dataset;

        } else {
            return null;
        }
    }

}
