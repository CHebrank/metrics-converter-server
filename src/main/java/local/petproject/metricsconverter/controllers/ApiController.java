package local.petproject.metricsconverter.controllers;

import local.petproject.metricsconverter.datasets.RawDataA;
import local.petproject.metricsconverter.datasets.RawDataU;
import local.petproject.metricsconverter.datasets.WosDataset;
import local.petproject.metricsconverter.fileIO.DtoPojo;
import local.petproject.metricsconverter.repositories.RawDataARepository;
import local.petproject.metricsconverter.repositories.RawDataURepository;
import local.petproject.metricsconverter.repositories.RepositoryService;
import local.petproject.metricsconverter.repositories.WosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class ApiController {

    int currentNumber = 51018;

    @Autowired
    RepositoryService repositoryService;

    @Autowired
    WosRepository wosRepository;

    @Autowired
    RawDataARepository rawDataARepository;

    @Autowired
    RawDataURepository rawDataURepository;

    @GetMapping("/dois")
    public String[] getDois() {
        int start = this.currentNumber + 1;
        int end = start + 1000;
        if (end > 107308) {
            end = 107308;
        }
        this.currentNumber = end;
        List<WosDataset> datasets = wosRepository.findWosDatasetsByIdBetween(start, end);
        String[] doiList = new String[datasets.size()];
        for (int i = 0; i < datasets.size(); i++) {
            doiList[i] = datasets.get(i).getDoi();
        }
        return doiList;
    }

    @PostMapping("/altmetric")
    public String saveRawDataA(@RequestBody DtoPojo dto) {
        for (int i = 0; i < dto.getLength(); i++) {
            RawDataA item = new RawDataA(dto.getData()[i]);
            try {
                rawDataARepository.save(item);
            } catch (Exception ex) {
                System.out.println(dto.getData()[i]);
                System.out.println(ex);
            }
            System.out.println("Last number of doi: " + this.currentNumber);
        }
        return "finished saving";
    }

    @PostMapping("/unpaywall")
    public String saveRawDataU(@RequestBody DtoPojo dto) {
        for (int i = 0; i < dto.getLength(); i++) {
            RawDataU item = new RawDataU(dto.getData()[i]);
            rawDataURepository.save(item);
        }
        return "finished saving";
    }

    @GetMapping("/convertA/{num}")
    public boolean convertDataA(@PathVariable(name = "num") String num) {
        //try {
            int start = Integer.valueOf(num);
            int end = start + 5000;
            if (end > 18276) {
                end = 18276;
            }
            repositoryService.convertRawDataA(start, end);
            return true;
        //} catch (Exception  ex) {
        //    System.out.println(ex);
        //   return false;
        //}
    }

    @GetMapping("/convertU/{num}")
    public boolean convertDataU(@PathVariable(name = "num") String num) {
        try {
            int start = Integer.valueOf(num);
            int end = start + 5000;
            if (end > 64894) {
                end = 64894;
            }
            repositoryService.convertRawDataU(start, end);
            return true;
        } catch (Exception ex) {
            System.out.println(ex);
            return false;
        }
    }
}
