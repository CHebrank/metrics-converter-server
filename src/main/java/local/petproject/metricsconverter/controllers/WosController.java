package local.petproject.metricsconverter.controllers;

import local.petproject.metricsconverter.datasets.WosDataset;
import local.petproject.metricsconverter.fileIO.DataImporter;
import local.petproject.metricsconverter.repositories.RepositoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.io.IOException;
import java.util.List;

@Controller
public class WosController {

    @Autowired
    RepositoryService repositoryService;

    @GetMapping("/wos")
    public String main() throws IOException {
        readWosDataIn();
        return "main";
    }

    public void readWosDataIn() throws IOException {
        int totalRead = 0;
        int totalConv = 0;
        for (int i = 1; i < 216; i++) {
            String filename = "src/main/resources/files/WoS-" + i + ".txt";
            System.out.println("Reading in file " + filename);

            DataImporter importer = new DataImporter(filename);
            List<String[]> basicData = importer.importDatasets();
            System.out.println(basicData.size() + " datasets were read in.");

            List<WosDataset> convertedData = importer.convertToWosDatasets(basicData);
            System.out.println(convertedData.size() + " datasets were converted.");

            totalRead += basicData.size();
            totalConv += convertedData.size();
            System.out.println("Total count: " + totalRead + " read, " + totalConv + " converted.");

            repositoryService.writeWosListsToDB(convertedData);
        }


    }

}
