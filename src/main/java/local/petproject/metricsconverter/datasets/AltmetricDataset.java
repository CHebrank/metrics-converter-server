package local.petproject.metricsconverter.datasets;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;


@Entity
public class AltmetricDataset {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;

    String doi;
    int cohortsPub;
    int cohortsSci;
    int cohortsCom;
    int cohortsDoc;

    int citedPosts;
    int citedDelicious;
    int citedFbwalls;
    int citedFeeds;
    int citedForum;
    int citedGplus;
    int citedLinkedin;
    int citedMsm;
    int citedPeerrev;
    int citedPinners;
    int citedPolicies;
    int citedQs;
    int citedRdts;
    int citedRh;
    int citedTweeters;
    int citedVideos;
    int citedWeibo;
    int citedWikipedia;

    Date lastUpdated;
    double score;
    double history1y;
    double history6m;
    double history3m;
    double historyAT;
    Date addedOn;

    int readersCount;
    int readerCiteULike;
    int readerMendeley;
    int readerConnotea;

    public AltmetricDataset() {
    }

    public AltmetricDataset(String doi) {
        this.doi = doi;
    }

    public String getDoi() {
        return doi;
    }

    public int getCohortsPub() {
        return cohortsPub;
    }

    public void setCohortsPub(int cohortsPub) {
        this.cohortsPub = cohortsPub;
    }

    public int getCohortsSci() {
        return cohortsSci;
    }

    public void setCohortsSci(int cohortsSci) {
        this.cohortsSci = cohortsSci;
    }

    public int getCohortsCom() {
        return cohortsCom;
    }

    public void setCohortsCom(int cohortsCom) {
        this.cohortsCom = cohortsCom;
    }

    public int getCohortsDoc() {
        return cohortsDoc;
    }

    public void setCohortsDoc(int cohortsDoc) {
        this.cohortsDoc = cohortsDoc;
    }

    public int getCitedPosts() {
        return citedPosts;
    }

    public void setCitedPosts(int citedPosts) {
        this.citedPosts = citedPosts;
    }

    public int getCitedDelicious() {
        return citedDelicious;
    }

    public void setCitedDelicious(int citedDelicious) {
        this.citedDelicious = citedDelicious;
    }

    public int getCitedFbwalls() {
        return citedFbwalls;
    }

    public void setCitedFbwalls(int citedFbwalls) {
        this.citedFbwalls = citedFbwalls;
    }

    public int getCitedFeeds() {
        return citedFeeds;
    }

    public void setCitedFeeds(int citedFeeds) {
        this.citedFeeds = citedFeeds;
    }

    public int getCitedForum() {
        return citedForum;
    }

    public void setCitedForum(int citedForum) {
        this.citedForum = citedForum;
    }

    public int getCitedGplus() {
        return citedGplus;
    }

    public void setCitedGplus(int citedGplus) {
        this.citedGplus = citedGplus;
    }

    public int getCitedLinkedin() {
        return citedLinkedin;
    }

    public void setCitedLinkedin(int citedLinkedin) {
        this.citedLinkedin = citedLinkedin;
    }

    public int getCitedMsm() {
        return citedMsm;
    }

    public void setCitedMsm(int citedMsm) {
        this.citedMsm = citedMsm;
    }

    public int getCitedPeerrev() {
        return citedPeerrev;
    }

    public void setCitedPeerrev(int citedPeerrev) {
        this.citedPeerrev = citedPeerrev;
    }

    public int getCitedPinners() {
        return citedPinners;
    }

    public void setCitedPinners(int citedPinners) {
        this.citedPinners = citedPinners;
    }

    public int getCitedPolicies() {
        return citedPolicies;
    }

    public void setCitedPolicies(int citedPolicies) {
        this.citedPolicies = citedPolicies;
    }

    public int getCitedQs() {
        return citedQs;
    }

    public void setCitedQs(int citedQs) {
        this.citedQs = citedQs;
    }

    public int getCitedRdts() {
        return citedRdts;
    }

    public void setCitedRdts(int citedRdts) {
        this.citedRdts = citedRdts;
    }

    public int getCitedRh() {
        return citedRh;
    }

    public void setCitedRh(int citedRh) {
        this.citedRh = citedRh;
    }

    public int getCitedTweeters() {
        return citedTweeters;
    }

    public void setCitedTweeters(int citedTweeters) {
        this.citedTweeters = citedTweeters;
    }

    public int getCitedVideos() {
        return citedVideos;
    }

    public void setCitedVideos(int citedVideos) {
        this.citedVideos = citedVideos;
    }

    public int getCitedWeibo() {
        return citedWeibo;
    }

    public void setCitedWeibo(int citedWeibo) {
        this.citedWeibo = citedWeibo;
    }

    public int getCitedWikipedia() {
        return citedWikipedia;
    }

    public void setCitedWikipedia(int citedWikipedia) {
        this.citedWikipedia = citedWikipedia;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public double getHistory1y() {
        return history1y;
    }

    public void setHistory1y(double history1y) {
        this.history1y = history1y;
    }

    public double getHistory6m() {
        return history6m;
    }

    public void setHistory6m(double history6m) {
        this.history6m = history6m;
    }

    public double getHistory3m() {
        return history3m;
    }

    public void setHistory3m(double history3m) {
        this.history3m = history3m;
    }

    public double getHistoryAT() {
        return historyAT;
    }

    public void setHistoryAT(double historyAT) {
        this.historyAT = historyAT;
    }

    public Date getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(Date addedOn) {
        this.addedOn = addedOn;
    }

    public int getReadersCount() {
        return readersCount;
    }

    public void setReadersCount(int readersCount) {
        this.readersCount = readersCount;
    }

    public int getReaderCiteULike() {
        return readerCiteULike;
    }

    public void setReaderCiteULike(int readerCiteULike) {
        this.readerCiteULike = readerCiteULike;
    }

    public int getReaderMendeley() {
        return readerMendeley;
    }

    public void setReaderMendeley(int readerMendeley) {
        this.readerMendeley = readerMendeley;
    }

    public int getReaderConnotea() {
        return readerConnotea;
    }

    public void setReaderConnotea(int readerConnotea) {
        this.readerConnotea = readerConnotea;
    }
}
