package local.petproject.metricsconverter.datasets;

import javax.persistence.*;

@Entity
public class RawDataA {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;

    @Lob
    String content;


    public RawDataA() {
    }

    public RawDataA(String content) {
        this.content = content;
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
