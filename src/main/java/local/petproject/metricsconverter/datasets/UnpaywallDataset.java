package local.petproject.metricsconverter.datasets;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class UnpaywallDataset {

    @Id
    String doi;

    String bestHostType;
    boolean bestIsBest;
    String bestLicense;
    String bestUpdated;
    String bestVersion;

    int dataStandard;
    boolean isOa;
    boolean journalDoaj;
    boolean journalOA;

    String publishedDate;
    String updated;



    public UnpaywallDataset(String doi) {
        this.doi = doi;
    }

    public UnpaywallDataset() {
    }

    public String getBestHostType() {
        return bestHostType;
    }

    public void setBestHostType(String bestHostType) {
        this.bestHostType = bestHostType;
    }

    public boolean isBestIsBest() {
        return bestIsBest;
    }

    public void setBestIsBest(boolean bestIsBest) {
        this.bestIsBest = bestIsBest;
    }

    public String getBestLicense() {
        return bestLicense;
    }

    public void setBestLicense(String bestLicense) {
        this.bestLicense = bestLicense;
    }

    public String getBestUpdated() {
        return bestUpdated;
    }

    public void setBestUpdated(String bestUpdated) {
        this.bestUpdated = bestUpdated;
    }

    public String getBestVersion() {
        return bestVersion;
    }

    public void setBestVersion(String bestVersion) {
        this.bestVersion = bestVersion;
    }

    public int getDataStandard() {
        return dataStandard;
    }

    public void setDataStandard(int dataStandard) {
        this.dataStandard = dataStandard;
    }

    public boolean isOa() {
        return isOa;
    }

    public void setOa(boolean oa) {
        isOa = oa;
    }

    public boolean isJournalDoaj() {
        return journalDoaj;
    }

    public void setJournalDoaj(boolean journalDoaj) {
        this.journalDoaj = journalDoaj;
    }

    public boolean isJournalOA() {
        return journalOA;
    }

    public void setJournalOA(boolean journalOA) {
        this.journalOA = journalOA;
    }

    public String getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(String publishedDate) {
        this.publishedDate = publishedDate;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getDoi() {
        return doi;
    }
}
