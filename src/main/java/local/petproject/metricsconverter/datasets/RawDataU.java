package local.petproject.metricsconverter.datasets;

import javax.persistence.*;

@Entity
public class RawDataU {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;

    @Lob
    String content;


    public RawDataU(String content) {
        this.content = content;
    }

    public RawDataU() {
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
