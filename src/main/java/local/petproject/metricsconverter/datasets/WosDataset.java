package local.petproject.metricsconverter.datasets;

import javax.persistence.*;

@Entity
public class WosDataset {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String publicationType;
    @Lob
    private String author;
    @Lob
    private String title;
    private String journal;
    private String docType;
    @Lob
    private String address;
    @Lob
    private String reprAddress;
    private int citedCountCCTCC;
    private int totalCitedCountTTCC;
    private String publisher;
    private String pubDate;
    private String pubYear;
    private String doi;
    private String subject;
    private String openAccessWoS;


    public long getId() {
        return id;
    }

    public String getPublicationType() {
        return publicationType;
    }

    public void setPublicationType(String publicationType) {
        this.publicationType = publicationType;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getJournal() {
        return journal;
    }

    public void setJournal(String journal) {
        this.journal = journal;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getReprAddress() {
        return reprAddress;
    }

    public void setReprAddress(String reprAddress) {
        this.reprAddress = reprAddress;
    }

    public int getCitedCountCCTCC() {
        return citedCountCCTCC;
    }

    public void setCitedCountCCTCC(int citedCountCCTCC) {
        this.citedCountCCTCC = citedCountCCTCC;
    }

    public int getTotalCitedCountTTCC() {
        return totalCitedCountTTCC;
    }

    public void setTotalCitedCountTTCC(int totalCitedCountTTCC) {
        this.totalCitedCountTTCC = totalCitedCountTTCC;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public String getPubYear() {
        return pubYear;
    }

    public void setPubYear(String pubYear) {
        this.pubYear = pubYear;
    }

    public String getDoi() {
        return doi;
    }

    public void setDoi(String doi) {
        this.doi = doi;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getOpenAccessWoS() {
        return openAccessWoS;
    }

    public void setOpenAccessWoS(String openAccessWoS) {
        this.openAccessWoS = openAccessWoS;
    }
}
