package local.petproject.metricsconverter.repositories;

import local.petproject.metricsconverter.datasets.AltmetricDataset;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AltmetricRepository extends JpaRepository<AltmetricDataset, Long> {

}
