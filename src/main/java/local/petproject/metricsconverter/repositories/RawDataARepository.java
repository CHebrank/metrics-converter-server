package local.petproject.metricsconverter.repositories;

import local.petproject.metricsconverter.datasets.RawDataA;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RawDataARepository extends JpaRepository<RawDataA,Long> {

    public List<RawDataA> findAllByIdBetween(long id1, long id2);

    public RawDataA findById(long id);

}
