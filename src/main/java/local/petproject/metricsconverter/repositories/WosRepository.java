package local.petproject.metricsconverter.repositories;

import local.petproject.metricsconverter.datasets.WosDataset;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface WosRepository extends JpaRepository<WosDataset, Long> {

    public List<WosDataset> findWosDatasetsByIdBetween(long start, long end);

    public WosDataset findById(long id);
}
