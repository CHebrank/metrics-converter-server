package local.petproject.metricsconverter.repositories;

import local.petproject.metricsconverter.datasets.RawDataU;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RawDataURepository extends JpaRepository<RawDataU, Long> {

    public List<RawDataU> findAll();

    public List<RawDataU> findAllByIdBetween(long id1, long id2);

}
