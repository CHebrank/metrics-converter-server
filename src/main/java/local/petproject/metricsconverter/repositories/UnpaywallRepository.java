package local.petproject.metricsconverter.repositories;

import local.petproject.metricsconverter.datasets.UnpaywallDataset;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UnpaywallRepository extends JpaRepository<UnpaywallDataset, String> {
}
