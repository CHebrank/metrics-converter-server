package local.petproject.metricsconverter.repositories;

import local.petproject.metricsconverter.datasets.*;
import local.petproject.metricsconverter.fileIO.DataConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public class RepositoryService {

    @Autowired
    WosRepository wosRepository;

    @Autowired
    RawDataARepository rawDataARepository;

    @Autowired
    RawDataURepository rawDataURepository;

    @Autowired
    AltmetricRepository altmetricRepository;

    @Autowired
    UnpaywallRepository unpaywallRepository;


    public void writeWosListsToDB(List<WosDataset> wosList) {
        for (int i = 0; i < wosList.size(); i++) {
            WosDataset currentDataset = wosList.get(i);
            wosRepository.save(currentDataset);
        }
    }

    /**
     * Converts raw data from JSON to AltmetricDataset and saves it to the database,
     * this includes reading the raw data from the database
     * @param start: id of first raw dataset to convert
     * @param end: id of last raw dataset to convert
     */
    public void convertRawDataA(int start, int end) {
        List<RawDataA> list = rawDataARepository.findAllByIdBetween(start, end);
        for (RawDataA item: list) {
            String[] array = DataConverter.getAttributeArray(item.getContent());
            HashMap<String, String> map = DataConverter.createMapping(array);
            AltmetricDataset amd = DataConverter.convertToAltmetric(map);
            altmetricRepository.save(amd);
        }
    }

    /**
     * Converts raw data from JSON to UnpaywallDataset and saves it to the database,
     * this includes reading the raw data from the database
     * @param start: id of first raw dataset to convert
     * @param end: id of last raw dataset to convert
     */
    public void convertRawDataU(int start, int end) {
        List<RawDataU> list = rawDataURepository.findAllByIdBetween(start, end);
        for (RawDataU item: list) {
            String[] array = DataConverter.getSimplifiedDataArrayU(item);
            if (array != null) {
                HashMap<String, String> map = DataConverter.createMapping(array);
                UnpaywallDataset upw = DataConverter.convertToUnpaywall(map);
                unpaywallRepository.save(upw);
            } else {
                System.out.println("error in getting array!");
            }
        }
    }

}
