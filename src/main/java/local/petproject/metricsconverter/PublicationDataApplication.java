package local.petproject.metricsconverter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PublicationDataApplication {

    public static void main(String[] args) {
        SpringApplication.run(PublicationDataApplication.class, args);

    }

}

