This project was created for my friend Lea Satzinger for use in her analysis of
altmetrics data.

It comprises a Java-server setup that can be used to create download lists for
Altmetric.com and Unpaywall-APIs based on DOIs obtained from a Web-of-Science
dataset. The JSON responses can be saved to a raw data database for each called 
API. Additionally, these JSON strings can be converted into datasets with the 
specified fields needed for analysis.
